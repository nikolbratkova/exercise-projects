import pandas as pd

# Creating table Ratings
Ratings = pd.read_csv('ratings.dat', sep='::', engine='python')
# Ratings.to_csv('ratings_df.csv', index=None)
Ratings.columns = ['userId', 'movieId', 'rating', 'timestamp']

# Creating table Movies
Movies = pd.read_csv('movies.dat', sep='::', engine='python')
Movies.to_csv('movies_df.csv', index=None)
Movies.columns = ['movieId', 'title', 'genres']

# Joining the tables into a new table MovieLens
left = Ratings
right = Movies
key = 'movieId'

MovieLens = pd.merge(left, right, on=key, how='left')
MovieLens.to_csv('movielens_df.csv', index=None)

# 2.
#counting rows 
rows_count = MovieLens.shape[0]
# rows_count = len(MovieLens.index) 
print(f'Count of rows in MovieLens: {rows_count}')

# 4.
# Get the count of Zeros in column 'Rating' 
count = (MovieLens['rating'] == 0).sum()
print(f'Count of zeros in Column Rating: {count}')

# 5.
# Get the count of threes in column 'Ratings'  
column = MovieLens['rating']
count = column[column == 3].count()
print(f'Count of threes in Column Rating: {count}')

# 6 and 7
unique_movies = MovieLens['movieId'].nunique()
unique_users = MovieLens['userId'].nunique()
print(f'Count of different movies: {unique_movies} \nCount of different users: {unique_users}')


# 8
count_drama = MovieLens['genres'].str.contains('Drama').value_counts()[True]
count_comedy= MovieLens['genres'].str.contains('Comedy').value_counts()[True]
count_thriller = MovieLens['genres'].str.contains('Thriller').value_counts()[True]
count_romance = MovieLens['genres'].str.contains('Romance').value_counts()[True]
print(f'Drama {count_drama}\nComedy {count_comedy}\nThriller {count_thriller}\nRomance {count_romance}')


# 9 Most greatest rating has Forrest Gump, Pulp Fiction, Speed 2: Cruise Control, Shawshank Redemption, Juror

# 10 most given ratings are from most to least
# edno ot tezi e verniq 
# 4,3.5,
# 3.5,4.5
# 0.5, 5
# 5,3
# 2, 3.5, 5